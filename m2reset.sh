#!/bin/bash
rm -rf var/cache/* var/page_cache/* var/generation/* var/di/* pub/static/* generated/* var/view_preprocessed/*
php ./bin/magento cache:clean
php ./bin/magento cache:flush
sudo chmod -R 777 ./pub/ ./var/ ./generated/
