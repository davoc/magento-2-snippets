#!/bin/bash
if [ ! -d 'vendor/magento/' ];then
	echo "Not a Magento 2 install. Exiting now."
	exit;
fi
sudo find . -type f -exec chmod 644 {} \;
sudo find . -type d -exec chmod 755 {} \;
sudo find ./var -type d -exec chmod 777 {} \;
sudo find ./pub/media -type d -exec chmod 777 {} \;
sudo find ./pub/static -type d -exec chmod 777 {} \;
sudo chmod 777 ./app/etc var/ generated/
sudo chmod 644 ./app/etc/*.xml
chown -R :www-data .
chmod u+x bin/magento
