# Upgrade Magento 2 #
Run the following command sequence (or create a shell script)
```
php bin/magento maintenance:enable
composer require magento/product-community-edition 2.2.6 --no-update
composer update
rm -rf var/cache/*
rm -rf var/page_cache/*
rm -rf var/generation/*
chmod +x bin/magento
php bin/magento setup:upgrade
php bin/magento maintenance:disable
```

# Upgrade To v2.3 #

Composer.json needs a few changes so composer update will fail. To fix:

```
composer config preferred-install dist
composer config sort-packages true
composer config prefer-stable true
composer require --dev friendsofphp/php-cs-fixer:~2.10.0 --no-update
composer require --dev sebastian/phpcpd:~3.0.0 --no-update
php -r '$autoload=json_decode(file_get_contents("composer.json"), true); $autoload["autoload"]["psr-4"]["Zend\\Mvc\\Controller\\"]= "setup/src/Zend/Mvc/Controller/"; file_put_contents("composer.json", json_encode($autoload, JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES));'
```
then run ```composer update```