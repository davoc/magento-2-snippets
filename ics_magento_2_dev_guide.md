# Icansee Magento 2 Developer Guide #

## Contents ##

1. Creating A Module
2. Collections
3. Factories
4. Api
5. Requirejs and Magento 2
6. Knockout.js

## 1. Creating A Module ##

All module files for Magento 2 reside in app/code

For a basic Magento 2 module you need a minimum of two files:

app/code/Vendor/Module/registration.php
```
<?php
    \Magento\Framework\Component\ComponentRegistrar::register(
        \Magento\Framework\Component\ComponentRegistrar::MODULE,
        'Vendor_Module',
        __DIR__
    );

```

app/code/Vendor/Module/etc/module.xml
```
<?xml version="1.0"?>
<config xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="urn:magento:framework:Module/etc/module.xsd">
    <module name="Vendor_Module" setup_version="0.0.1"/>
</config>

```

After this, in order to enable the module you can run ```php bin/magento setup:upgrade``` and this will install and enable the module.

Your module's functionality and requirements will determine what other components you will need to create. This document outlines the more common components that can be created for Magento 2 modules.
