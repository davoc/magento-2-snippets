# Using Newsletter As Consent In Forms #

Magento 2 has a built in newsletter subscribe module. We can use this table as our GDPR consent for marketing table. Here's how to do it via a contact form:

Firstly, set up your contact form and add a checkbox field
```
<input type="checkbox" name="consent" value="1"/>
```
Then in your controller, create function:
```
protected $subscriberFactory;

public function __construct(
    ...
    \Magento\Newsletter\Model\SubscriberFactory $subscriberFactory,
    ...
    )
{
    ...
    $this->subscriberFactory= $subscriberFactory;
    ...
}

private function setCustomerConsent($email)
{
    if (!$this->subscriberFactory->create()->loadByEmail($email)) {
        $this->subscriberFactory->create()->subscribe($email);
    }
}
```
You can then use the feature like so:
```
try {
    if ($consent == 1) {
        $this->setCustomerConsent($email);
    }
} catch (\Exception $e) {
    echo $e->getMessage();
}
```
Check the newsletter_subscriber table for the new record. This is the table you can get email addresses from for generating marketing lists for email campaigns.
