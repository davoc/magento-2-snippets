# Remove a Module From Magento 2 #
Installing module in Magento 2 is far easier than uninstalling Magento 2 module(s) so today we are going to cover how to uninstall Magento 2 modules from your webstore without leaving any trace of that module on your filesystem and database 🙂
Here is the step to step guide to uninstall any third party module in Magento 2 manually.

1: Remove the module Vendor_% from app/etc/config.php

2: Drop module tables or columns from database, please check app/code/Vendor/Module/Setup folder for more information

3: Remove the folder app/code/vendor/%

4: Remove module configuration settings from core_config_data table by running the following query
```
    DELETE FROM core_config_data WHERE path LIKE 'vendor%';
```
5: Remove module from setup_module table by running the following query
```
    DELETE FROM setup_module WHERE module LIKE 'vendor_%';
```
6: Run the following command by logging onto your SSH server
```
    php bin/magento setup:upgrade
```
But if you have installed the module via composer then you can run the following list of commands by SSHing on the box to uninstall third party module
```
    php bin/magento module:uninstall -r Vendor_ModuleName
```
-r flag removes module data then run the following command
```
    php bin/magento setup:upgrade
```
