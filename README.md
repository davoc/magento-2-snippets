# Bash Utilities For Magento 2 #

## Instructions ##

### Clear caches and reset file permissions with m2rst ###

- add the alias in .bash_aliases to your own .bash_aliases file (or create the file in your home directory). Ensure you edit the user folder to your own username!

- Copy the m2reset.sh file to your home directory (the same filepath used in the previous step)

```
chmod +x ~/m2reset.sh
```

That's it. To use the script, cd to your magento root directory and execute it:

```
m2rst
```

### Reset all file permissions correctly with m2fpr ###

- Copy filesreset.sh to your home directory and make it executable

- Add a line to your .bash_aliases file with whatever command name you want to call it - I use m2fpr for this.

- Open a new terminal and change to a Magento 2 install then execute your command

### Further Info ###

I'll be creating more scripts in this repo for Magento 2 development, as well as modifying and adding parameters to the existing utilities, so watch the repo and if you find you're typing in the same chain of commands all the time, create a .sh script and make it executable, add an alias command to your .bash_aliases file to execute the script and that's that.
