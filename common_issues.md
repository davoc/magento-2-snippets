# Magento 2 Development Issues #

## Current Bugs and fixes ##

### Currently, there is an issue with Full Page Caching and the quote object. ###

If you are trying to get the summary, totals, item count or other information and there is either zero or null value being returned (assuming you have your code correct) you need to fix this using the layout.xml file.

Find the block tag and add the attribute ```cacheable="false"``` to access cart quote in your custom module.

### When adding a customer address, there is an issue with the validation of the region_id field from the dropdown in the form. ###

To fix this error, in www/vendor/magento/module-customer/view/base/ui_component/customer_form.xml the region_id is marked with an unconditional required validator:
```
        <field name="region_id">
            <argument name="data" xsi:type="array">
                <item name="config" xsi:type="array">
                    <item name="dataType" xsi:type="string">text</item>
                    <item name="formElement" xsi:type="string">select</item>
                    <item name="source" xsi:type="string">address</item>
                    <item name="customEntry" xsi:type="string">region</item>
                    <item name="validation" xsi:type="array">
                        <item name="required-entry" xsi:type="boolean">true</item>
                    </item>
                    <item name="filterBy" xsi:type="array">
                        <item name="target" xsi:type="string">${ $.provider }:${ $.parentScope }.country_id</item>
                        <item name="field" xsi:type="string">country_id</item>
                    </item>
                </item>
            </argument>
        </field>
```
removing this validator "fixes" the issue - obviously the region is then also not required for countries where no "State is Required for" opt-out exists.

To do this, override this layout file http://devdocs.magento.com/guides/v2.0/frontend-dev-guide/layouts/layout-override.html#fedg_layout_override_default and remove the validation item from the xml

## Admin Panel css, js and icons 404 ##

You will have to deploy static content for admin panel manually as the "aggressive strategy" of optimisation fails.

Firstly, edit the database:
```
select * from core_config_data where path like '%dev/static/sign%'
```

If a row exists, change the value to 0. If not, create the row:
```
INSERT INTO `core_config_data` (`scope`, `scope_id`, `path`, `value`) VALUES ('default', 0, 'dev/static/sign', '0');
```

Secondly, deploy static cintent for admin area manually like so:
```
setup:static-content:deploy --force --area adminhtml --theme Magento/backend
setup:static-content:deploy --force --area adminhtml --theme Magento/backend en_GB
setup:static-content:deploy --force --area adminhtml --theme Magento/backend en_US
```

## Disable all magento 2 modules for a particular vendor name ##

```
php bin/magento module:status | grep [VendorName] | grep -v List | grep -v None | grep -v -e '^$' | xargs php bin/magento module:disable
```

## Migrate Database Commands ##

Migrate in the order:

1. Settings
2. Data
3. Changes

### Settings ###
```
php bin/magento migrate:settings -ar /var/www/vhosts/balti-migration/vendor/magento/data-migration-tool/etc/opensource-to-opensource/1.6.2.0/config.xml
```

### Data ###
```
php bin/magento migrate:data -a /var/www/vhosts/balti-migration/vendor/magento/data-migration-tool/etc/opensource-to-opensource/1.6.2.0/config.xml
```

### Changes ###
```
php bin/magento migrate:delta -a /var/www/vhosts/balti-migration/vendor/magento/data-migration-tool/etc/opensource-to-opensource/1.6.2.0/config.xml

```

## Disable all magento 2 modules for a particular vendor name ##
```
php bin/magento module:status | grep 'Icansee' | grep -v List | grep -v None | grep -v -e '^$' | xargs php bin/magento module:disable
```
## Disable all custom modules except Magento's core modules ##
```
php bin/magento module:status | grep -v Magento | grep -v List | grep -v None | grep -v -e '^$'| xargs php bin/magento module:disable
```

## Fix AllMethods.php Carriers Array Error ##

If you get an error when trying to access shipping methods in admin area which says
```
1 exception(s):
Exception #0 (Exception): Notice: Array to string conversion in vendor/magento/module-shipping/Model/Config/Source/Allmethods.php on line 61
```
This is a current open bug in Magento 2 https://github.com/magento/magento2/issues/13136
A temp fix is as follows:
In file vendor/magento/module-shipping/Model/Config/Source/Allmethods.php change
```
$methods[$carrierCode]['value'][] = [
'value' => $carrierCode . '_' . $methodCode,
'label' => '[' . $carrierCode . '] ' . $methodTitle,
];
```
to this:
```
if(!is_array($methodTitle))
$methods[$carrierCode]['value'][] = [
'value' => $carrierCode . '_' . $methodCode,
'label' => '[' . $carrierCode . '] ' . $methodTitle,
];
```
## Incorrect Date Format Error When Saving Products In Admin ##

This is due to the order of the day/month fields and Magento'2 inability to read the datetime string in the correct format for any locale other than en_US. To fix, log into the admin area and navigate to Stores > Config > Catalog > Catalog > Date & Time Custom Options. Set the order of the day/month and year settings correctly as per the locale used.

## Get Stacktrace ##

f you receive the error message "No such entity.", "No such entity with" or "No such entity with customerId" in Magento 2, the issue usually occurred when you try to load not existing object via Magento 2 Repository Class.

To debug this issue, please open the file
```
vendor/magento/framework/Exception/NoSuchEntityException.php
```
and at the beginning of the ```__construct``` method temporary add debug backtrace code:

```
foreach (debug_backtrace() as $_stack) {
        echo ($_stack["file"] ? $_stack["file"] : '') . ':' .
            ($_stack["line"] ? $_stack["line"] : '') . ' - ' .
            ($_stack["function"] ? $_stack["function"] : '');
     }
    exit();
```

Example:

```
public function __construct(Phrase $phrase = null, \Exception $cause = null, $code = 0)
 {

    foreach (debug_backtrace() as $_stack) {
        echo ($_stack["file"] ? $_stack["file"] : '') . ':' .
            ($_stack["line"] ? $_stack["line"] : '') . ' - ' .
            ($_stack["function"] ? $_stack["function"] : '');
     }
    exit();


    if ($phrase === null) {
        $phrase = new Phrase('No such entity.');
    }
    parent::__construct($phrase, $cause, $code);
 }
```
Save the file and refresh the page.
You will see the debug backtrace that will allow you to define the issue source and you will get an idea how to fix it.
Usually third party Magento 2 extensions lead up to this problem, so you can override their code and add "try-catch" exception.
Don't forget to revert changes in NoSuchEntityException.php file after you're finished.

## Cannot Save Products In Admin Due To Invalid Date Format Error ##

Common issue with Magento 2 and datetime formatting. There is a module that fixes this here: https://github.com/fernandofauth/magento2_datetime


## Cannot Save Products In Admin Due To Error With End Date < Start Date ##

To fix: Go to table eav_attribute find attribute_code 'custom_design_from' or 'custom_design_to', 'news_from_date' or 'news_to_date' then you will get attribute id. Now open table catalog_product_entity_datetime search by attribute_id you will see datetime value wrong, please correct it or delete. If this doesn't exist or work, I have fixed this before in MySQL:
```
    TRUNCATE TABLE catalog_product_entity_datetime;
```

Also, check in product > advanced pricing to see if the special price from and to dates are correctly set. If they are wrong in bulk you can easily remove them from the database. Since we use flat products for performance, the columns are (conveniently enough) special_price_from and special_price_to in the flat catalog_product_flat_1 table (or just catalog_product_flat, or possibly catalog_product_flat_store_1, depending on the Magento version).

## Cannot Change Log Settings After Deploying in Production Mode ##

Sometimes, even after changing back to developer mode, the "Log To File" setting is disabled in the admin area and cannot be changed. To change the value:
```
php bin/magento config:set dev/debug/debug_logging 1 --lock
```

## Admin Grids Not Rendering Correctly ##

This is an issue where Magento 2 cannot find the static generated files. Three things will fix this. First, make sure you apply the fix described above, regarding Symlink (AWS server will not follow symlinks for security reasons so you need to make hard links to the static files). Secondly, turn off static file signing in the admin area (Stores > Configuration > Developer > Static Files). Third, disable Modpagespeed in the Apache server directives. (AWS server do this through Plesk panel in Apache/Nginx settings).

## PHP Fatal error:  Uncaught Error: Call to a member function getPackage() on null ##

in /var/www/vhosts/.../vendor/magento/module-deploy/Package/Processor/PostProcessor/CssUrls.php:215

This is caused by previously genereated cache on var/view_preprocessed, to fix this we need to remove cache:
```
rm -rf pub/static/*
rm -rf var/view_preprocessed/pub
```

## (Magento\Framework\Exception\LocalizedException): Unable to find a physical ancestor for a theme ##

You have done a database migration and the site's frontend will not load, displaying the above error. To fix, go to the database, theme table. Find the theme that you are trying to use. Change the theme type from 1 (virtual) to 0 (physical). This happens because you haven't done the migration in a vanilla install.

## Add pagination To Page ##

Add this to the phtml file and set the number of products to display in the correct admin configuration settings.
```
<?php if ($block->getPagerHtml()): ?>
    <div class="order-products-toolbar toolbar bottom"><?php echo $block->getPagerHtml(); ?></div>
<?php endif; ?>
```
